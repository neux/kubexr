#! /bin/bash

# Get the output of git describe --tags
DESCRIBE=$(git describe --tags)

# Extract the main version number, commit count, and hash (if present)
if [[ $DESCRIBE =~ ^v([0-9]+(\.[0-9]+)*)(-([0-9]+)-g[0-9a-f]+)?$ ]]; then
    MAIN_VERSION=${BASH_REMATCH[1]}
    COMMIT_COUNT=${BASH_REMATCH[4]}
else
    echo "Error: Git tag format not recognized." >&2
    exit 1
fi

# Construct the semantic version
SEMVER=$MAIN_VERSION
if [ -n "$COMMIT_COUNT" ] && [ "$COMMIT_COUNT" -ne 0 ]; then
    SEMVER+=".$COMMIT_COUNT"
fi

echo $SEMVER
