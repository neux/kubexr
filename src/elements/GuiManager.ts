import { GUI } from 'lil-gui';
import { XRHandlerBase } from './XRHandlerBase';
import { GUIFolder, ParamConfig, StorageType } from '../models/storage';
import { CreativeMode } from './CreativeMode';

type TransformType = 'position' | 'rotation' | 'scale';

interface TransformParamClickedEvent {
    instanceId: string;
    paramType: TransformType;
    param: string;
}

interface TransformParamChangedEvent {
    instanceId: string;
    paramType: TransformType;
    param: string;
    value: any;
}

export class GuiManager {
    private guiInstance: GUI | null = null;
    private active: boolean = false;
    private currentInstance: XRHandlerBase | null = null;
    private folderMap: Map<string, GUI> = new Map();

    private transformParamClickedListeners: ((event: TransformParamClickedEvent) => void)[] = [];
    private transformParamChangedListeners: ((event: TransformParamChangedEvent) => void)[] = [];

    private saveInstanceListeners: ((instance: XRHandlerBase) => void)[] = [];

    constructor(private creativeMode: CreativeMode) {}

    async toggleGUI(instance: XRHandlerBase | null): Promise<void> {
        console.log(`Toggling GUI for instance: ${instance ? instance.getTypeId() : 'null'}, Current active state: ${this.active}`);
        
        if (instance === null) {
            this.hideGUI();
            return;
        }

        if (this.active && this.currentInstance === instance) {
            this.hideGUI();
        } else {
            await this.showGUI(instance);
        }
    }

    private async showGUI(instance: XRHandlerBase): Promise<void> {
        console.log("Setting up GUI");
        this.currentInstance = instance;
        const typeDefinition = instance.getStorageType();
        if (!typeDefinition || !typeDefinition.gui_folders || typeDefinition.gui_folders.length === 0) {
            console.warn(`No GUI folders found for type: ${instance.getTypeId()}`);
            return;
        }

        const instanceData = instance.getInstanceData();
        console.log("Instance data:", instanceData);

        this.guiInstance = await this.setupGUIControls(typeDefinition, instanceData, instance);

        console.log("GUI setup complete");
        this.active = true;
    }

    private hideGUI(): void {
        if (this.guiInstance) {
            this.guiInstance.destroy();
            this.guiInstance = null;
        }
        this.active = false;
        this.currentInstance = null;
        this.folderMap.clear();
        console.log("GUI hidden");
    }

    public updateGUI(instance: XRHandlerBase): void {
        if (!this.guiInstance || !this.active) {
            console.warn("GUI is not active, cannot update");
            return;
        }

        const typeDefinition = instance.getStorageType();
        if (!typeDefinition) {
            console.warn("No type definition found for instance");
            return;
        }

        const instanceData = instance.getInstanceData();
        console.log("Updating GUI with instance data:", instanceData);

        typeDefinition.gui_folders.forEach(folderConfig => {
            if (folderConfig.id !== "storage") {
                const folder = this.folderMap.get(folderConfig.id);
                if (folder) {
                    this.createRegularFolder(folder, folderConfig, instanceData, instance);
                }
            }
        });
    }

    private async setupGUIControls(typeDefinition: StorageType, instanceData: any, instance: XRHandlerBase): Promise<GUI> {
        console.log("Setting up GUI controls");

        let GUIConstructor: typeof GUI;
        try {
            const lilGui = await import('lil-gui');
            GUIConstructor = lilGui.default || lilGui.GUI;
            
            if (typeof GUIConstructor !== 'function') {
                throw new Error('GUI constructor not found in lil-gui module');
            }
        } catch (error) {
            console.error('Failed to load or use lil-gui:', error);
            throw new Error(`Unable to create GUI instance: ${error instanceof Error ? error.message : String(error)}`);
        }

        const gui = new GUIConstructor({ title: typeDefinition.gui_name });

        this.createGUIFolders(gui, typeDefinition.gui_folders, instanceData, instance);

        // Add global change handler
        gui.onChange((event) => {
            console.log("GUI changed:", event);
            if (this.currentInstance) {
                this.currentInstance.updateParam(event.property, event.value);
            }
        });

        console.log("GUI setup complete");
        return gui;
    }

    private createGUIFolders(gui: GUI, guiFolders: GUIFolder[], instanceData: any, instance: XRHandlerBase) {
        console.log('Creating GUI folders with instance data:', instanceData);

        guiFolders.forEach(folderConfig => {
            const folder = gui.addFolder(folderConfig.name);
            this.folderMap.set(folderConfig.id, folder);
            
            if (folderConfig.id === "storage") {
                this.createPersistenceFolder(folder, folderConfig, instance);
            } else {
                this.createRegularFolder(folder, folderConfig, instanceData, instance);
            }
        });
    }

    private createRegularFolder(folder: GUI, folderConfig: GUIFolder, instanceData: any, instance: XRHandlerBase) {
        folderConfig.params.forEach((param: ParamConfig) => {
            let value = instanceData[param.param] ?? param.default;
            console.log(`Adding/Updating param: ${param.param}, value: ${value}, type: ${param.type}`);
            
            let controller = folder.controllers.find(c => c.property === param.param);
            
            if (!controller) {
                try {
                    switch (param.type) {
                        case 'textbox':
                            controller = folder.add({ [param.param]: value }, param.param);
                            break;
                        case 'dropdown':
                            if ('options' in param && Array.isArray(param.options)) {
                                controller = folder.add({ [param.param]: value }, param.param, param.options);
                            }
                            break;
                        case 'checkbox':
                            controller = folder.add({ [param.param]: value }, param.param);
                            break;
                        default:
                            controller = folder.add({ [param.param]: value }, param.param, param.min, param.max, param.step);
                    }
                    
                    if (controller) {
                        controller.onChange((value: any) => this.handleChange(param.param, value, instance));
                        controller.onFinishChange((value: any) => this.handleFinishChange(param.param, value, instance, folderConfig.id));
                        
                        // Add click event for transform parameters
                        if (this.isTransformParam(param.param)) {
                            controller.domElement.addEventListener('click', () => {
                                const paramType = this.getTransformType(param.param);
                                this.emitTransformParamClicked({
                                    instanceId: instance.getTypeId(),
                                    paramType: paramType,
                                    param: param.param
                                });
                            });
                        }
                    }
                } catch (error) {
                    console.error(`Error adding param ${param.param}:`, error);
                }
            } else {
                controller.setValue(value);
            }
        });
    }

    private createPersistenceFolder(folder: GUI, folderConfig: GUIFolder, instance: XRHandlerBase) {
        folderConfig.params.forEach((param: ParamConfig) => {
            switch (param.param) {
                case 'id':
                    const storageInstance = instance.getStorageInstance();
                    const instanceId = storageInstance ? storageInstance.id : 'Unknown';
                    folder.add({ id: instanceId }, 'id').disable();
                    break;
                case 'Save':
                    folder.add({ Save: () => this.handleSave(instance) }, 'Save');
                    break;
                case 'Delete':
                    folder.add({ Delete: () => this.handleDeleteInstance() }, 'Delete');
                    break;
                default:
                    console.warn(`Unexpected param in Storage folder: ${param.param}`);
            }
        });
    }

    private handleChange(param: string, value: any, instance: XRHandlerBase) {
        instance.updateParam(param, value);
    }

    private handleFinishChange(param: string, value: any, instance: XRHandlerBase, folderId?: string) {
        instance.updateParam(param, value);
        
        if (this.isTransformParam(param)) {
            this.emitTransformParamChanged({
                instanceId: instance.getTypeId(),
                paramType: this.getTransformType(param),
                param: param,
                value: value
            });
        }
    }

    private isTransformParam(param: string): boolean {
        return param.startsWith('pos_') || param.startsWith('rotation_') || param.startsWith('scale_');
    }

    private getTransformType(param: string): TransformType {
        if (param.startsWith('pos_')) return 'position';
        if (param.startsWith('rotation_')) return 'rotation';
        if (param.startsWith('scale_')) return 'scale';
        throw new Error(`Invalid transform parameter: ${param}`);
    }

    // Event handling methods
    onTransformParamClicked(listener: (event: TransformParamClickedEvent) => void) {
        this.transformParamClickedListeners.push(listener);
    }

    onTransformParamChanged(listener: (event: TransformParamChangedEvent) => void) {
        this.transformParamChangedListeners.push(listener);
    }

    private emitTransformParamClicked(event: TransformParamClickedEvent) {
        this.transformParamClickedListeners.forEach(listener => listener(event));
    }

    private emitTransformParamChanged(event: TransformParamChangedEvent) {
        this.transformParamChangedListeners.forEach(listener => listener(event));
    }

    private handleSave(instance: XRHandlerBase) {
        console.log("Saving instance");
        instance.saveStorageInstance();
        this.emitSaveInstance(instance);
    }

    onSaveInstance(listener: (instance: XRHandlerBase) => void) {
        this.saveInstanceListeners.push(listener);
    }

    private emitSaveInstance(instance: XRHandlerBase) {
        this.saveInstanceListeners.forEach(listener => listener(instance));
    }

    // private handleNew(instance: XRHandlerBase) {
    //     console.log("Creating new instance");
    //     this.xrStorage.createInstance(instance.getTypeId());
    // }

    private handleDeleteInstance(): void {
        if (this.currentInstance) {
            const storageInstance = this.currentInstance.getStorageInstance();
            if (storageInstance) {
                console.log(`Deleting instance with id: ${storageInstance.id} of type: ${this.currentInstance.getTypeId()}`);
                this.creativeMode.deleteInstance(storageInstance.id);
                this.hideGUI();
            }
        } else {
            console.warn("No active instance to delete");
        }
    }

    // Comment out the unused method
    /*
    private handleDeleteInstance(instance: XRHandlerBase): void {
        const storageInstance = instance.getStorageInstance();
        if (storageInstance) {
            console.log(`Deleting instance with id: ${storageInstance.id}`);
            this.xrStorage.deleteInstance(storageInstance.id);
        }
    }
    */

    // private handleAddInstance(instance: XRHandlerBase): void {
    //     console.log(`Adding new instance of type: ${instance.getTypeId()}`);
    //     this.xrStorage.createInstance(instance.getTypeId());
    // }

    dispose() {
        if (this.guiInstance) {
            this.guiInstance.destroy();
            this.guiInstance = null;
        }
        this.active = false;
        this.currentInstance = null;
    }
}
