import * as BABYLON from '@babylonjs/core';

export class GizmoManager {
    private utilLayer: BABYLON.UtilityLayerRenderer;
    private positionGizmo: BABYLON.PositionGizmo | null = null;
    private rotationGizmo: BABYLON.RotationGizmo | null = null;
    private scaleGizmo: BABYLON.ScaleGizmo | null = null;
    private boundingBoxGizmo: BABYLON.BoundingBoxGizmo | null = null;
    private activeGizmoType: 'position' | 'rotation' | 'scale' | 'boundingBox' | null = null;
    private attachedMesh: BABYLON.AbstractMesh | null = null;
    public onGizmoMoved: (position: BABYLON.Vector3, rotation: BABYLON.Vector3, scaling: BABYLON.Vector3) => void = () => {};

    constructor(scene: BABYLON.Scene) {
        console.log("GizmoManager constructor called");
        this.utilLayer = new BABYLON.UtilityLayerRenderer(scene);
        this.utilLayer.utilityLayerScene.autoClearDepthAndStencil = false;
        this.createGizmos();
    }

    private createGizmos(): void {
        this.positionGizmo = new BABYLON.PositionGizmo(this.utilLayer);
        this.rotationGizmo = new BABYLON.RotationGizmo(this.utilLayer);
        this.scaleGizmo = new BABYLON.ScaleGizmo(this.utilLayer);
        this.boundingBoxGizmo = new BABYLON.BoundingBoxGizmo(BABYLON.Color3.FromHexString("#00A0B0"), this.utilLayer);

        [this.positionGizmo, this.rotationGizmo, this.scaleGizmo].forEach(gizmo => {
            if (gizmo) {
                gizmo.updateGizmoPositionToMatchAttachedMesh = true;
                gizmo.snapDistance = 0.5;
                gizmo.scaleRatio = 1.5;
                gizmo.attachedMesh = null;
            }
        });

        if (this.boundingBoxGizmo) {
            this.boundingBoxGizmo.scaleRatio = 1.5;
            this.boundingBoxGizmo.attachedMesh = null;
            this.setupBoundingBoxGizmoObservables();
        }

        this.setupGizmoObservables();
    }

    private setupGizmoObservables(): void {
        [this.positionGizmo, this.rotationGizmo, this.scaleGizmo].forEach(gizmo => {
            if (gizmo) {
                gizmo.onDragStartObservable.add(() => {
                    console.log(`Gizmo drag started`);
                });

                gizmo.onDragEndObservable.add(() => {
                    console.log(`Gizmo drag ended`);
                    if (this.attachedMesh) {
                        const position = this.attachedMesh.position;
                        const rotation = this.attachedMesh.rotation;
                        const scaling = this.attachedMesh.scaling;
                        
                        console.log(`Gizmo moved: Position(${position.x.toFixed(2)}, ${position.y.toFixed(2)}, ${position.z.toFixed(2)}), Rotation(${rotation.x.toFixed(2)}, ${rotation.y.toFixed(2)}, ${rotation.z.toFixed(2)}), Scaling(${scaling.x.toFixed(2)}, ${scaling.y.toFixed(2)}, ${scaling.z.toFixed(2)})`);
                        
                        this.onGizmoMoved(position, rotation, scaling);
                    }
                });
            }
        });
    }

    private setupBoundingBoxGizmoObservables(): void {
        if (this.boundingBoxGizmo) {
            this.boundingBoxGizmo.onRotationSphereDragObservable.add(() => {
                console.log("Bounding box rotation dragging");
            });

            this.boundingBoxGizmo.onRotationSphereDragEndObservable.add(() => {
                console.log("Bounding box rotation ended");
                if (this.attachedMesh) {
                    const position = this.attachedMesh.position;
                    const rotation = this.attachedMesh.rotationQuaternion 
                        ? this.attachedMesh.rotationQuaternion.toEulerAngles() 
                        : this.attachedMesh.rotation;
                    const scaling = this.attachedMesh.scaling;
                    console.log(`Bounding box rotated: Rotation(${rotation.x.toFixed(2)}, ${rotation.y.toFixed(2)}, ${rotation.z.toFixed(2)})`);
                    this.onGizmoMoved(position, rotation, scaling);
                }
            });

            this.boundingBoxGizmo.onScaleBoxDragObservable.add(() => {
                console.log("Bounding box scaling");
            });

            this.boundingBoxGizmo.onScaleBoxDragEndObservable.add(() => {
                console.log("Bounding box scaling ended");
                if (this.attachedMesh) {
                    const position = this.attachedMesh.position;
                    const rotation = this.attachedMesh.rotationQuaternion 
                        ? this.attachedMesh.rotationQuaternion.toEulerAngles() 
                        : this.attachedMesh.rotation;
                    const scaling = this.attachedMesh.scaling;
                    console.log(`Bounding box scaled: Scaling(${scaling.x.toFixed(2)}, ${scaling.y.toFixed(2)}, ${scaling.z.toFixed(2)})`);
                    this.onGizmoMoved(position, rotation, scaling);
                }
            });
        }
    }

    public attachToMesh(mesh: BABYLON.AbstractMesh | null): void {
        console.log(`GizmoManager: Attaching to mesh: ${mesh ? mesh.name : 'null'}`);
        this.attachedMesh = mesh;
        [this.positionGizmo, this.rotationGizmo, this.scaleGizmo].forEach(gizmo => {
            if (gizmo) {
                gizmo.attachedMesh = mesh;
            }
        });
        this.setActiveGizmo(mesh ? 'position' : null);
    }

    public attachBoundingBoxGizmo(mesh: BABYLON.AbstractMesh): void {
        console.log(`GizmoManager: Attaching bounding box gizmo to mesh: ${mesh ? mesh.name : 'null'}`);
        this.attachedMesh = mesh;
        if (this.boundingBoxGizmo) {
            this.boundingBoxGizmo.attachedMesh = mesh;
        }
        this.setActiveGizmo('boundingBox');
    }

    public detachBoundingBoxGizmo(): void {
        if (this.boundingBoxGizmo) {
            this.boundingBoxGizmo.attachedMesh = null;
        }
    }

    public detachAllGizmos(): void {
        [this.positionGizmo, this.rotationGizmo, this.scaleGizmo, this.boundingBoxGizmo].forEach(gizmo => {
            if (gizmo) {
                gizmo.attachedMesh = null;
            }
        });
        this.attachedMesh = null;
        this.activeGizmoType = null;
    }

    public updateGizmoPositions(): void {
        if (this.attachedMesh) {
            if (this.positionGizmo && this.positionGizmo.attachedMesh) {
                this.positionGizmo.updateGizmoPositionToMatchAttachedMesh = true;
            }
            if (this.rotationGizmo && this.rotationGizmo.attachedMesh) {
                this.rotationGizmo.updateGizmoRotationToMatchAttachedMesh = true;
            }
            if (this.scaleGizmo && this.scaleGizmo.attachedMesh) {
                this.scaleGizmo.updateGizmoPositionToMatchAttachedMesh = true;
            }
            if (this.boundingBoxGizmo && this.boundingBoxGizmo.attachedMesh) {
                this.boundingBoxGizmo.updateBoundingBox();
            }
        }
    }

    public setActiveGizmo(type: 'position' | 'rotation' | 'scale' | 'boundingBox' | null): void {
        this.activeGizmoType = type;
        this.updateActiveGizmo();
    }

    private updateActiveGizmo(): void {
        [this.positionGizmo, this.rotationGizmo, this.scaleGizmo, this.boundingBoxGizmo].forEach(gizmo => {
            if (gizmo) {
                gizmo.attachedMesh = null;
            }
        });

        let activeGizmo: BABYLON.Gizmo | null = null;
        switch (this.activeGizmoType) {
            case 'position':
                activeGizmo = this.positionGizmo;
                break;
            case 'rotation':
                activeGizmo = this.rotationGizmo;
                break;
            case 'scale':
                activeGizmo = this.scaleGizmo;
                break;
            case 'boundingBox':
                activeGizmo = this.boundingBoxGizmo;
                break;
        }

        if (activeGizmo && this.attachedMesh) {
            activeGizmo.attachedMesh = this.attachedMesh;
        }
    }

    public dispose(): void {
        [this.positionGizmo, this.rotationGizmo, this.scaleGizmo, this.boundingBoxGizmo].forEach(gizmo => {
            if (gizmo) {
                gizmo.dispose();
            }
        });
        this.utilLayer.dispose();
    }

    public getActiveGizmoType(): 'position' | 'rotation' | 'scale' | 'boundingBox' | null {
        return this.activeGizmoType;
    }
}

