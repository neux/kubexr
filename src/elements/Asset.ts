export class Asset {
    id: string;
    k_dimension_x: number;
    k_dimension_y: number;
    k_dimension_z: number;
    // Add other properties as per your asset structure

    constructor(assetData: any) {
        this.id = assetData.id;
        this.k_dimension_x = assetData.k_dimension_x;
        this.k_dimension_y = assetData.k_dimension_y;
        this.k_dimension_z = assetData.k_dimension_z;
        // Initialize other properties
    }
}
