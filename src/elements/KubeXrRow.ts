// KubeXrRow.ts
import { KubeXrKube } from './KubeXrKube';
import { Assets } from './Assets';

export class KubeXrRow {
  kubes: KubeXrKube[];

  constructor(kubeRow: string, kz: number, ky: number, assets: Assets) {
    this.kubes = [];
    this.buildRow(kubeRow, kz, ky, assets);
  }

  private buildRow(
    kubeRow: string,
    kz: number,
    ky: number,
    assets: Assets
  ): void {
    const kubeCodes = kubeRow.split(' ');
    for (let kx = 0; kx < kubeCodes.length; kx++) {
      const kubeCode = kubeCodes[kx].split('-')[0]; // Assuming ID is before the dash
      const rotation = parseInt(kubeCodes[kx].split('-')[1]); // Assuming rotation is after the dash
      const asset = assets.getAssetById(kubeCode);
      if (asset) {
        this.kubes.push(new KubeXrKube(kubeCode, kx, ky, kz, rotation, asset));
      }
    }
  }
}
