// KubeXrMap.ts
import { KubeXrLevel } from './KubeXrLevel';
import { Assets } from './Assets';

export class KubeXrMap {
  levels: KubeXrLevel[];
  mapData: any;
  assets: Assets;

  constructor(mapData: any, assets: Assets) {
    this.mapData = mapData;
    this.assets = assets;
    this.levels = [];
    this.buildLevels();
  }

  private buildLevels(): void {
    if (!Array.isArray(this.mapData.map)) {
      console.error("Invalid map data: Expected 'map' to be an array.");
      return;
    }

    for (const level of this.mapData.map) {
      try {
        const levelKeys = Object.keys(level);
        if (levelKeys.length !== 1 || !Array.isArray(level[levelKeys[0]])) {
          throw new Error('Malformed level data.');
        }

        const levelKey = levelKeys[0]; // e.g., "L0", "L1"
        this.levels.push(
          new KubeXrLevel(
            level[levelKey],
            parseInt(levelKey.substring(1)),
            this.assets
          )
        );
      } catch (error) {
        if (error instanceof Error) {
          console.error(`Error parsing level: ${error.message}`);
        } else {
          console.error('Unknown error occurred while parsing level.');
        }
      }
    }
  }
}
