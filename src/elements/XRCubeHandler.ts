// XRCubeHandler.ts
import * as BABYLON from '@babylonjs/core';
import { XRHandlerBase } from './XRHandlerBase';
import { XRStorage } from './XRStorage';
import { StorageInstance } from '../models/storage';

export class XRCubeHandler extends XRHandlerBase {
    private guiVisible: boolean = false;

    constructor(scene: BABYLON.Scene, xrstorage: XRStorage) {
        super(scene, xrstorage);
    }

    getTypeId(): string {
        return 'xrcube';
    }

    async createInstanceMesh(instance: StorageInstance): Promise<BABYLON.AbstractMesh> {
        console.log(`Creating cube instance: ${instance.id}`);
        return BABYLON.MeshBuilder.CreateBox(instance.id, { size: 1 }, this.scene);
    }

    updateInstanceMesh(mesh: BABYLON.AbstractMesh, params: any): void {
        mesh.position = new BABYLON.Vector3(params.pos_x || 0, params.pos_y || 0, params.pos_z || 0);
        mesh.rotation = new BABYLON.Vector3(
            BABYLON.Tools.ToRadians(params.rotation_x || 0),
            BABYLON.Tools.ToRadians(params.rotation_y || 0),
            BABYLON.Tools.ToRadians(params.rotation_z || 0)
        );
        mesh.scaling = new BABYLON.Vector3(params.scale_x || 1, params.scale_y || 1, params.scale_z || 1);

        console.log(`Cube updated at position: (${mesh.position.x}, ${mesh.position.y}, ${mesh.position.z})`);
    }

    hideGui(): void {
        // Implement hide GUI logic
        this.guiVisible = false;
        console.log("XRCubeHandler: GUI hidden");
    }

    showGui(): void {
        // Implement show GUI logic
        this.guiVisible = true;
        console.log("XRCubeHandler: GUI shown");
    }

    toggleGui(): void {
        this.guiVisible = !this.guiVisible;
        console.log(`XRCubeHandler: GUI toggled, now ${this.guiVisible ? 'visible' : 'hidden'}`);
    }

    isGuiVisible(): boolean {
        return this.guiVisible;
    }
}