import * as BABYLON from '@babylonjs/core';
import { XRCubeHandler } from './XRCubeHandler';
import { XRImageHandler } from './XRImageHandler';
import { XRHandlerBase } from './XRHandlerBase';
import { XRStorage } from './XRStorage';
import { CreativeMode } from './CreativeMode';
import { StorageInstance } from '../models/storage';

export class SceneManager {
    private hInstances: Map<string, XRHandlerBase> = new Map();
    private creativeMode: CreativeMode | null = null;
    private isCreativeMode: boolean;
    private saveInstanceListeners: ((instance: XRHandlerBase) => void)[] = [];

    constructor(private xrstorage: XRStorage, private scene: BABYLON.Scene, isCreativeMode: boolean) {
        console.log("SceneManager constructor called");
        
        this.isCreativeMode = isCreativeMode;
        this.initializeHandledInstances();
        
        this.scene.executeWhenReady(() => {
            console.log("Scene is ready in SceneManager");
            if (this.isCreativeMode) {
                this.enableCreativeMode();
            }
            console.log("Scene is ready and CreativeMode initialized if needed");
        });
        
        console.log("SceneManager initialized");
    }

    private async initializeHandledInstances(): Promise<void> {
        console.log("Initializing handled instances");
        const instances = this.xrstorage.storage.instances;
        for (const instance of instances) {
            await this.createHandledInstance(instance);
        }
        console.log("Handled instances initialized");
    }

    private async createHandledInstance(instance: StorageInstance): Promise<void> {
        console.log(`Creating handled instance for type: ${instance.type}`);
        const handler = this.createHandlerForType(instance.type);
        if (handler) {
            await handler.loadStorageInstance(instance);
            this.hInstances.set(instance.id, handler);
            console.log(`Handled instance created for id: ${instance.id}`);
        }
    }

    private createHandlerForType(type: string): XRHandlerBase | null {
        switch (type) {
            case 'xrcube':
                return new XRCubeHandler(this.scene, this.xrstorage);
            case 'xrimage':
                return new XRImageHandler(this.scene, this.xrstorage);
            // Add more cases for future types here
            default:
                console.error(`Unknown instance type: ${type}`);
                return null;
        }
    }

    public toggleCreativeMode(type: string): void {
        console.log(`Toggling creative mode for type: ${type}`);
        if (!this.isCreativeMode) {
            this.isCreativeMode = true;
            this.enableCreativeMode();
            this.creativeMode?.openInstanceEditorGUI(type);
        } else {
            this.isCreativeMode = false;
            this.disableCreativeMode();
        }
    }

    private enableCreativeMode(): void {
        console.log("Enabling creative mode");
        if (!this.creativeMode) {
            this.creativeMode = new CreativeMode(this.scene, this.xrstorage, this.hInstances, this);
            this.scene.onPointerDown = (evt, pickResult) => {
                this.creativeMode?.onPointerDown(evt, pickResult);
            };
            console.log("CreativeMode enabled");
        }
    }

    private disableCreativeMode(): void {
        console.log("Disabling creative mode");
        if (this.creativeMode) {
            this.creativeMode.dispose();
            this.creativeMode = null;
            this.scene.onPointerDown = undefined;
            console.log("CreativeMode disabled");
        }
    }

    public createInstance(type: string): void {
        this.creativeMode?.createInstance(type);
    }

    public getHandlerByType(type: string): XRHandlerBase | undefined {
        return Array.from(this.hInstances.values()).find(h => h.getTypeId() === type);
    }

    public getHandlerById(id: string): XRHandlerBase | undefined {
        return this.hInstances.get(id);
    }

    public getAllHandlers(): Map<string, XRHandlerBase> {
        return this.hInstances;
    }

    public handleSaveInstance = (instance: XRHandlerBase): void => {
        this.emitSaveInstance(instance);
    }

    public onSaveInstance(listener: (instance: XRHandlerBase) => void): void {
        this.saveInstanceListeners.push(listener);
    }

    private emitSaveInstance(instance: XRHandlerBase): void {
        this.saveInstanceListeners.forEach(listener => listener(instance));
    }

    // Remove or comment out this method as it's no longer needed here
    // private logStorageInstances(): void { ... }
}