// KubeXrKube.ts
import { Asset } from './Asset';

export const UNIVERSAL_K_UNIT = 2.5;

export class KubeXrKube {
  public k_position_x: number;
  public k_position_y: number;
  public k_position_z: number;
  public u_position_x: number = 0; // Initialized with default value
  public u_position_y: number = 0; // Initialized with default value
  public u_position_z: number = 0; // Initialized with default value
  public asset: Asset;

  constructor(
    kubeCode: string,
    kx: number,
    ky: number,
    kz: number,
    rotation: number,
    asset: Asset
  ) {
    // Store the asset
    this.asset = asset;

    // Store K-coordinates
    this.k_position_x = kx;
    this.k_position_y = ky;
    this.k_position_z = kz;

    // Calculate and store U-coordinates
    this.calculateUPositions(rotation);
    this.initializeKube(kubeCode);
  }

  private calculateUPositions(rotation: number): void {
    let offsetX = 0;
    let offsetZ = 0;

    // Adjust for rotation
    switch (rotation) {
      case 1: // 90 degrees
        offsetX = this.asset.k_dimension_x * UNIVERSAL_K_UNIT;
        break;
      case 2: // 180 degrees
        offsetX = this.asset.k_dimension_x * UNIVERSAL_K_UNIT;
        offsetZ = this.asset.k_dimension_z * UNIVERSAL_K_UNIT;
        break;
      case 3: // 270 degrees
        offsetZ = this.asset.k_dimension_z * UNIVERSAL_K_UNIT;
        break;
    }

    this.u_position_x = this.k_position_x * UNIVERSAL_K_UNIT - offsetX;
    this.u_position_y = this.k_position_y * UNIVERSAL_K_UNIT;
    this.u_position_z = this.k_position_z * UNIVERSAL_K_UNIT - offsetZ;
  }

  private initializeKube(kubeCode: string): void {
    // Initialization logic for a single kube
    console.log(
      kubeCode,
      this.u_position_x,
      this.u_position_y,
      this.u_position_z
    );
  }
}
