import * as BABYLON from '@babylonjs/core';
import { StorageType, StorageInstance, ParamConfig, GUIFolder } from '../models/storage';
import { storageXrCube } from '../models/storageXrCube';
import { storageXrImage } from '../models/storageXrImage';

export class XRStorage {
    storage: {
        types: {
            [key: string]: StorageType;
        };
        instances: StorageInstance[];
    } = {
        types: {
            xrcube: storageXrCube,
            xrimage: storageXrImage
        },
        instances: []
    };

    loadFromJsonString(jsonString: string): void {
        try {
            const parsedData = JSON.parse(jsonString);
            if (!parsedData.instances || !Array.isArray(parsedData.instances)) {
                throw new Error('Invalid JSON format. Expected an "instances" array at the root level.');
            }

            this.storage.instances = parsedData.instances.map((item: any) => this.validateAndCreateInstance(item)).filter(Boolean) as StorageInstance[];
        } catch (error) {
            console.error('Error parsing JSON:', error);
        }
    }

    private validateAndCreateInstance(data: any): StorageInstance | null {
        if (!data.id || !data.type || !data.params) {
            console.error('Invalid instance data. Missing required fields.');
            return null;
        }

        const typeDefinition = this.getTypeDefinition(data.type);
        if (!typeDefinition) {
            console.error(`Type '${data.type}' is not defined in the storage system.`);
            return null;
        }

        const instance: StorageInstance = {
            id: data.id,
            type: data.type,
            params: {}
        };

        typeDefinition.gui_folders.forEach(folder => {
            folder.params.forEach(param => {
                if (param.output !== false) {
                    if (data.params.hasOwnProperty(param.param)) {
                        instance.params[param.param] = data.params[param.param];
                    } else {
                        instance.params[param.param] = param.default ?? 0;
                    }
                }
            });
        });

        return instance;
    }

    private padWithZeros(num: number, targetLength: number): string {
        const numStr = num.toString();
        const zerosToAdd = Math.max(0, targetLength - numStr.length);
        return '0'.repeat(zerosToAdd) + numStr;
    }

    createNewId(type: string): string {
        const existingIds = this.storage.instances
            .filter(instance => instance.type === type)
            .map(instance => {
                const match = instance.id.match(/-(\d+)$/);
                return match ? parseInt(match[1], 10) : 0;
            });

        let nextNumber = 1;
        while (existingIds.some(id => id === nextNumber)) {
            nextNumber++;
        }

        return `${type}-${this.padWithZeros(nextNumber, 3)}`;
    }

    createInstance(type: string): StorageInstance | null {
        const typeDefinition = this.getTypeDefinition(type);
        if (!typeDefinition) {
            console.error(`Type '${type}' is not defined in the storage system.`);
            return null;
        }

        const id = this.createNewId(type);
        const instance: StorageInstance = {
            id: id,
            type: type,
            params: {}
        };

        typeDefinition.gui_folders.forEach(folder => {
            folder.params.forEach(param => {
                if (param.output !== false) {
                    instance.params[param.param] = param.default ?? 0;
                }
            });
        });

        this.storage.instances.push(instance);
        return instance;
    }

    getInstance(id: string): StorageInstance | undefined {
        return this.storage.instances.find(instance => instance.id === id);
    }

    updateInstance(id: string, newValues: Record<string, any>): void {
        const instance = this.getInstance(id);
        if (instance) {
            Object.keys(newValues).forEach(key => {
                if (key in instance.params) {
                    instance.params[key] = newValues[key];
                }
            });
        }
    }

    deleteInstance(id: string): void {
        this.storage.instances = this.storage.instances.filter(instance => instance.id !== id);
    }

    saveInstance(instanceId: string): void {
        const instance = this.getInstance(instanceId);
        if (instance) {
            console.log(`Saving instance: ${instanceId}`);
            const serializedData = JSON.stringify(this.storage, null, 2);
            console.log(serializedData);
            console.log(`Instance ${instanceId} saved successfully.`);
        } else {
            console.warn(`Instance with id ${instanceId} not found. Unable to save.`);
        }
    }

    getTypeDefinition(type: string): StorageType | undefined {
        return this.storage.types[type];
    }

    getTypeIds(): string[] {
        return Object.keys(this.storage.types);
    }

    // Remove or comment out the following method if it's not used elsewhere
    // addInstance(type: string, initialValues: any = {}): StorageInstance | null {
    //     const name = `${type}-${this.storage.instances.length}`;
    //     return this.createInstance(type);
    // }

    updateInstanceParam(id: string, param: string, value: any): void {
        const instance = this.getInstance(id);
        if (instance) {
            instance.params[param] = value;
        }
    }

    getInstanceParams(id: string): Record<string, ParamConfig> | null {
        const instance = this.getInstance(id);
        if (!instance) return null;

        const typeDefinition = this.getTypeDefinition(instance.type);
        if (!typeDefinition) return null;

        const paramsObject: Record<string, ParamConfig> = {};
        typeDefinition.gui_folders.forEach(folder => {
            folder.params.forEach(param => {
                paramsObject[param.param] = param;
            });
        });

        return paramsObject;
    }

    getInstancesByType(type: string): StorageInstance[] {
        return this.storage.instances.filter(instance => instance.type === type);
    }

    getInstanceCreationParams(type: string, camera: BABYLON.Camera): Record<string, any> {
        const typeDefinition = this.getTypeDefinition(type);
        const params: Record<string, any> = {};

        if (typeDefinition) {
            typeDefinition.gui_folders.forEach(folder => {
                folder.params.forEach(param => {
                    if (param.output !== false) {
                        params[param.param] = param.default ?? 0;
                    }
                });
            });
        }

        if (params.on_camera_axis) {
            const invViewMatrix = camera.getViewMatrix().invert();
            const rotationQuaternion = BABYLON.Quaternion.FromRotationMatrix(invViewMatrix);
            const rotation = rotationQuaternion.toEulerAngles();

            params.rotation_x = BABYLON.Tools.ToDegrees(rotation.x);
            params.rotation_y = BABYLON.Tools.ToDegrees(rotation.y) + 180;
            params.rotation_z = BABYLON.Tools.ToDegrees(rotation.z);
        }

        return params;
    }

    getGUIFolderByName(type: string, folderName: string): GUIFolder | undefined {
        const typeDefinition = this.getTypeDefinition(type);
        return typeDefinition?.gui_folders.find(folder => folder.name === folderName);
    }

    loadFromPanelInputs(inputs: Record<string, any>): void {
        try {
            if (!inputs.instances || !Array.isArray(inputs.instances)) {
                throw new Error('Invalid input format. Expected an "instances" array.');
            }

            this.storage.instances = inputs.instances.map((item: any) => this.validateAndCreateInstance(item)).filter(Boolean) as StorageInstance[];
        } catch (error) {
            console.error('Error processing panel inputs:', error);
        }
    }

    getInstances(): StorageInstance[] {
        return this.storage.instances.map(instance => this.filterDefaultValues(instance));
    }

    private filterDefaultValues(instance: StorageInstance): StorageInstance {
        const typeDefinition = this.getTypeDefinition(instance.type);
        if (!typeDefinition) {
            return instance; // If no type definition found, return the instance as is
        }

        const filteredParams: Record<string, any> = {};

        for (const key in instance.params) {
            if (instance.params.hasOwnProperty(key)) {
                let value = instance.params[key];
                const paramConfig = this.findParamConfig(typeDefinition, key);
                if (paramConfig) {
                    // Round numeric values to 6 decimal places
                    if (typeof value === 'number') {
                        value = Number(value.toFixed(6));
                    }
                    if (value !== paramConfig.default) {
                        filteredParams[key] = value;
                    }
                }
            }
        }

        return {
            ...instance,
            params: filteredParams
        };
    }

    private findParamConfig(typeDefinition: StorageType, paramName: string): ParamConfig | undefined {
        for (const folder of typeDefinition.gui_folders) {
            const param = folder.params.find(p => p.param === paramName);
            if (param) {
                return param;
            }
        }
        return undefined;
    }

    // ... rest of the code ...
}
