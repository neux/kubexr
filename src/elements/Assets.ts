import { Asset } from './Asset';

export class Assets {
    private assets: Map<string, Asset>;

    constructor(assetsData: any[]) {
        this.assets = new Map();
        this.loadAssets(assetsData);
    }

    private loadAssets(assetsData: any[]): void {
        assetsData.forEach(assetData => {
            if (assetData.type === "3d.kubexr.gltf") { // Filter by type
                const asset = new Asset(assetData);
                this.assets.set(asset.id, asset);
            }
        });
    }

    getAssetById(id: string): Asset | undefined {
        return this.assets.get(id);
    }
}
