import * as BABYLON from '@babylonjs/core';
import { XRStorage } from './XRStorage';
import { GuiManager } from './GuiManager';
import { XRHandlerBase } from './XRHandlerBase';
import { GizmoManager } from './GizmoManager';
import { StorageInstance } from '../models/storage';
import { XRCubeHandler } from './XRCubeHandler';
import { XRImageHandler } from './XRImageHandler';
import { SceneManager } from './SceneManager';

export class CreativeMode {
    private gizmoManager: GizmoManager;
    private guiManager: GuiManager;
    private activeHInstance: XRHandlerBase | null = null;
    private activeMesh: BABYLON.AbstractMesh | null = null;
    private lastRotation: BABYLON.Quaternion | null = null;

    constructor(private scene: BABYLON.Scene, private xrStorage: XRStorage, private hInstances: Map<string, XRHandlerBase>, private sceneManager: SceneManager) {
        this.gizmoManager = new GizmoManager(this.scene);
        this.guiManager = new GuiManager(this);

        this.setupMeshClickHandlers();
        this.setupGizmoMovedCallback();
        this.setupGuiManagerListeners();

        // Update this line to pass the save event to SceneManager
        this.guiManager.onSaveInstance(this.sceneManager.handleSaveInstance.bind(this.sceneManager));
    }

    private setupMeshClickHandlers(): void {
        this.scene.onPointerObservable.add((pointerInfo) => {
            if (pointerInfo.type === BABYLON.PointerEventTypes.POINTERDOWN) {
                const pickedMesh = pointerInfo.pickInfo?.pickedMesh;
                if (pickedMesh) {
                    this.handleMeshClick(pickedMesh);
                } else {
                    this.deactivateGizmoAndGui();
                }
            }
        });
    }

    private handleMeshClick(mesh: BABYLON.AbstractMesh): void {
        const hInstance = this.findHInstanceForMesh(mesh);
        if (hInstance) {
            if (this.activeHInstance === hInstance && this.activeMesh === mesh) {
                // Clicking on the same instance, do nothing
                return;
            }
            this.activateGizmoAndGui(mesh, hInstance);
        } else {
            this.deactivateGizmoAndGui();
        }
    }

    private activateGizmoAndGui(mesh: BABYLON.AbstractMesh, hInstance: XRHandlerBase): void {
        this.deactivateGizmoAndGui(); // Deactivate previous instance if any
        this.activeHInstance = hInstance;
        this.activeMesh = mesh;
        this.gizmoManager.attachBoundingBoxGizmo(mesh);
        this.guiManager.toggleGUI(hInstance);
    }

    private deactivateGizmoAndGui(): void {
        if (this.activeHInstance) {
            this.gizmoManager.detachAllGizmos();
            this.guiManager.toggleGUI(null);
            this.activeHInstance = null;
            this.activeMesh = null;
        }
    }

    private findHInstanceForMesh(mesh: BABYLON.AbstractMesh): XRHandlerBase | null {
        for (const hInstance of this.hInstances.values()) {
            if (hInstance.getMesh() === mesh) {
                return hInstance;
            }
        }
        return null;
    }

    private setupGizmoMovedCallback(): void {
        this.gizmoManager.onGizmoMoved = (position: BABYLON.Vector3, rotation: BABYLON.Vector3, scaling: BABYLON.Vector3) => {
            if (this.activeHInstance && this.activeMesh) {
                console.log(`Gizmo moved for instance ${this.activeHInstance.getTypeId()}:`, { position, rotation, scaling });
                
                let finalRotation: BABYLON.Vector3;

                if (this.gizmoManager.getActiveGizmoType() === 'rotation') {
                    // Handle rotation gizmo
                    if (this.activeMesh.rotationQuaternion) {
                        finalRotation = this.activeMesh.rotationQuaternion.toEulerAngles();
                    } else if (this.activeMesh.rotation) {
                        finalRotation = this.activeMesh.rotation;
                    } else {
                        console.warn('Unable to determine rotation for mesh');
                        finalRotation = new BABYLON.Vector3(0, 0, 0);
                    }
                } else {
                    // Handle other gizmo types
                    finalRotation = this.activeMesh.rotationQuaternion 
                        ? this.activeMesh.rotationQuaternion.toEulerAngles() 
                        : (this.activeMesh.rotation || new BABYLON.Vector3(0, 0, 0));
                }

                this.updateMeshAndInstance(position, finalRotation, scaling);
            }
        };
    }

    private updateMeshAndInstance(position: BABYLON.Vector3, rotation: BABYLON.Vector3, scaling: BABYLON.Vector3): void {
        if (this.activeHInstance && this.activeMesh) {
            // Update the mesh
            this.activeMesh.position.copyFrom(position);
            
            if (this.activeMesh.rotationQuaternion) {
                this.activeMesh.rotationQuaternion = BABYLON.Quaternion.FromEulerVector(rotation);
            } else {
                this.activeMesh.rotation = rotation;
            }
            
            this.activeMesh.scaling.copyFrom(scaling);

            // Update the XRHandlerBase instance
            this.activeHInstance.updateTransform(position, rotation, scaling);

            // Update GUI
            this.guiManager.updateGUI(this.activeHInstance);
        }
    }

    private setupGuiManagerListeners(): void {
        this.guiManager.onTransformParamClicked((event) => {
            const logMessage = this.getLogMessage(event.paramType, event.param);
            console.log(logMessage);
            this.setGizmoType(event.instanceId, event.paramType);
        });

        this.guiManager.onTransformParamChanged((event) => {
            this.updateTransformGizmo(event.instanceId, event.paramType, event.param, event.value);
        });
    }

    private getLogMessage(paramType: 'position' | 'rotation' | 'scale', param: string): string {
        switch (paramType) {
            case 'position':
                return `Focus on ${param} detected. Activating positional gizmo`;
            case 'rotation':
                return `Focus on ${param} detected. Activating rotational gizmo`;
            case 'scale':
                return `Focus on ${param} detected. Activating scale gizmo`;
        }
    }

    private setGizmoType(instanceId: string, paramType: 'position' | 'rotation' | 'scale'): void {
        console.log(`Switching to ${paramType} gizmo for instance: ${instanceId}`);
        if (this.gizmoManager && this.activeMesh) {
            this.gizmoManager.detachBoundingBoxGizmo();
            this.gizmoManager.attachToMesh(this.activeMesh);
            this.gizmoManager.setActiveGizmo(paramType);

            if (paramType === 'position') {
                // Store current rotation when switching to position gizmo
                this.lastRotation = this.activeMesh.rotationQuaternion 
                    ? this.activeMesh.rotationQuaternion.clone() 
                    : BABYLON.Quaternion.FromEulerVector(this.activeMesh.rotation);
            } else if (paramType === 'rotation' && this.lastRotation) {
                // Restore the last rotation when switching back to rotation gizmo
                this.activeMesh.rotationQuaternion = this.lastRotation.clone();
                this.lastRotation = null; // Clear the stored rotation
            }
        }
    }

    private updateTransformGizmo(instanceId: string, paramType: 'position' | 'rotation' | 'scale', param: string, value: any): void {
        console.log(`Updating ${paramType} gizmo for instance: ${instanceId}, param: ${param}, value: ${value}`);
        if (this.activeHInstance && this.activeHInstance.getTypeId() === instanceId && this.activeMesh) {
            const axis = param.charAt(param.length - 1).toLowerCase() as 'x' | 'y' | 'z';
            const newPosition = this.activeMesh.position.clone();
            const newRotation = this.activeMesh.rotation.clone();
            const newScaling = this.activeMesh.scaling.clone();

            switch (paramType) {
                case 'position':
                    newPosition[axis] = value;
                    break;
                case 'rotation':
                    newRotation[axis] = BABYLON.Tools.ToRadians(value);
                    break;
                case 'scale':
                    newScaling[axis] = value;
                    break;
            }

            this.updateMeshAndInstance(newPosition, newRotation, newScaling);
        }
    }

    public dispose(): void {
        this.deactivateGizmoAndGui();
        this.guiManager.dispose();
        this.gizmoManager.dispose();
    }

    public openInstanceEditorGUI(type: string): void {
        // Implement the logic to open the instance editor GUI
        console.log(`Opening instance editor GUI for type: ${type}`);
        // Add your implementation here
    }

    public onPointerDown(evt: BABYLON.IPointerEvent, pickResult: BABYLON.PickingInfo): void {
        // Implement the logic to handle pointer down events
        console.log('Pointer down event', evt, pickResult);
        // Add your implementation here
    }

    public calculateNewInstancePositionRotation(camera: BABYLON.Camera): { position: BABYLON.Vector3, rotation: BABYLON.Vector3 } {
        // Calculate position 5 units away from camera
        const cameraDirection = camera.getDirection(BABYLON.Vector3.Forward());
        const position = camera.position.add(cameraDirection.scale(5));

        // Calculate rotation to face the camera
        const rotationQuaternion = BABYLON.Quaternion.FromLookDirectionRH(cameraDirection.scale(-1), BABYLON.Vector3.Up());
        const rotation = rotationQuaternion.toEulerAngles();

        return { position, rotation };
    }

    public createInstance(type: string): void {
        const camera = this.scene.activeCamera as BABYLON.Camera;
        if (!camera) {
            console.error("No active camera found");
            return;
        }

        const newInstance = this.xrStorage.createInstance(type);
        if (newInstance) {
            const { position, rotation } = this.calculateNewInstancePositionRotation(camera);
            
            // Update the instance with calculated position and rotation
            newInstance.params.pos_x = position.x;
            newInstance.params.pos_y = position.y;
            newInstance.params.pos_z = position.z;
            newInstance.params.rotation_x = BABYLON.Tools.ToDegrees(rotation.x);
            newInstance.params.rotation_y = BABYLON.Tools.ToDegrees(rotation.y);
            newInstance.params.rotation_z = BABYLON.Tools.ToDegrees(rotation.z);

            this.createHandledInstance(newInstance);
        }
    }

    private async createHandledInstance(instance: StorageInstance): Promise<void> {
        console.log(`Creating handled instance for type: ${instance.type}`);
        const handler = this.createHandlerForType(instance.type);
        if (handler) {
            await handler.loadStorageInstance(instance);
            this.hInstances.set(instance.id, handler);
            console.log(`Handled instance created for id: ${instance.id}`);
        }
    }

    private createHandlerForType(type: string): XRHandlerBase | null {
        switch (type) {
            case 'xrcube':
                return new XRCubeHandler(this.scene, this.xrStorage);
            case 'xrimage':
                return new XRImageHandler(this.scene, this.xrStorage);
            // Add more cases for future types here
            default:
                console.error(`Unknown instance type: ${type}`);
                return null;
        }
    }

    public deleteInstance(id: string): void {
        console.log(`Attempting to delete instance with id: ${id}`);
        
        const handler = this.hInstances.get(id);
        if (handler) {
            // Disengage gizmos and GUI
            this.deactivateGizmoAndGui();

            // Remove mesh from scene
            const mesh = handler.getMesh();
            if (mesh) {
                mesh.dispose();
            }

            // Remove from hInstances
            this.hInstances.delete(id);

            // Remove from storage
            this.xrStorage.deleteInstance(id);

            console.log(`Instance with id ${id} of type ${handler.getTypeId()} has been deleted`);

            // Clear the active instance and mesh
            this.activeHInstance = null;
            this.activeMesh = null;
        } else {
            console.warn(`No instance found with id: ${id}`);
        }
    }
}