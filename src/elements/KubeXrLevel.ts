// KubeXrLevel.ts
import { KubeXrRow } from './KubeXrRow';
import { Assets } from './Assets';

export class KubeXrLevel {
  rows: KubeXrRow[];
  levelIndex: number;
  assets: Assets;

  constructor(levelData: string[], levelIndex: number, assets: Assets) {
    this.rows = [];
    this.levelIndex = levelIndex;
    this.assets = assets;
    this.buildLevel(levelData);
  }

  private buildLevel(levelData: string[]): void {
    for (let i = levelData.length - 1; i >= 0; i--) {
      this.rows.push(
        new KubeXrRow(
          levelData[i],
          this.levelIndex,
          levelData.length - 1 - i,
          this.assets
        )
      );
    }
  }
}
