import * as BABYLON from '@babylonjs/core';
import { XRStorage } from './XRStorage';
import { StorageInstance, StorageType } from '../models/storage';

export abstract class XRHandlerBase {
    protected mesh: BABYLON.AbstractMesh | null = null;
    protected storageInstance: StorageInstance | null = null;
    protected storageType: StorageType | null = null;

    constructor(protected scene: BABYLON.Scene, protected xrstorage: XRStorage) {
        const typeDefinition = this.xrstorage.getTypeDefinition(this.getTypeId());
        this.storageType = typeDefinition || null;
        if (!this.storageType) {
            console.error(`Failed to get type definition for ${this.getTypeId()}`);
        }
    }

    abstract getTypeId(): string;
    abstract createInstanceMesh(instance: StorageInstance): Promise<BABYLON.AbstractMesh>;
    abstract updateInstanceMesh(mesh: BABYLON.AbstractMesh, params: any): void;

    async loadStorageInstance(instance: StorageInstance): Promise<BABYLON.AbstractMesh> {
        this.storageInstance = this.loadInstanceWithDefaults(instance);
        const mesh = await this.createInstanceMesh(this.storageInstance);
        this.updateInstanceMesh(mesh, this.storageInstance.params);
        this.mesh = mesh;
        return mesh;
    }

    public getMeshes(): BABYLON.AbstractMesh[] {
        return this.mesh ? [this.mesh] : [];
    }

    public getMesh(): BABYLON.AbstractMesh | null {
        const meshes = this.getMeshes();
        return meshes.length > 0 ? meshes[0] : null;
    }

    public getStorageInstance(): StorageInstance | null {
        return this.storageInstance;
    }

    public getStorageType(): StorageType | null {
        return this.storageType;
    }

    public updateParam(param: string, value: any): void {
        if (this.storageInstance && this.mesh) {
            this.storageInstance.params[param] = value;
            this.updateInstanceMesh(this.mesh, this.storageInstance.params);
        }
    }

    protected loadInstanceWithDefaults(instance: StorageInstance): StorageInstance {
        if (!this.storageType) {
            console.warn(`No storage type found for ${this.getTypeId()}, using instance as-is`);
            return instance;
        }

        const defaultParams: Record<string, any> = {};
        
        this.storageType.gui_folders.forEach(folder => {
            folder.params.forEach(param => {
                if (param.default !== undefined) {
                    defaultParams[param.param] = param.default;
                }
            });
        });

        return {
            ...instance,
            params: { ...defaultParams, ...instance.params }
        };
    }

    public saveStorageInstance(): StorageInstance | null {
        if (!this.storageInstance || !this.storageType) return null;
        
        const savedParams: Record<string, any> = {};
        
        this.storageType.gui_folders.forEach(folder => {
            folder.params.forEach(param => {
                if (this.storageInstance && this.storageInstance.params[param.param] !== undefined) {
                    savedParams[param.param] = this.storageInstance.params[param.param];
                }
            });
        });

        const updatedInstance = {
            ...this.storageInstance,
            params: savedParams
        };

        this.xrstorage.updateInstance(updatedInstance.id, updatedInstance.params);
        return updatedInstance;
    }

    abstract hideGui(): void;
    abstract showGui(): void;
    abstract toggleGui(): void;
    abstract isGuiVisible(): boolean;

    public getInstanceData(): any {
        if (this.storageInstance) {
            return this.storageInstance.params;
        } else {
            console.warn('Attempted to get instance data, but storageInstance is null');
            return {};
        }
    }

    public updateTransform(position: BABYLON.Vector3, rotation: BABYLON.Vector3, scaling: BABYLON.Vector3): void {
        if (this.mesh) {
            this.mesh.position.copyFrom(position);
            this.mesh.rotation.copyFrom(rotation);
            this.mesh.scaling.copyFrom(scaling);
            this.updateInstanceData();
        }
    }

    protected updateInstanceData(): void {
        if (this.storageInstance && this.storageType) {
            const mesh = this.getMesh();
            if (mesh) {
                this.storageInstance.params.pos_x = mesh.position.x;
                this.storageInstance.params.pos_y = mesh.position.y;
                this.storageInstance.params.pos_z = mesh.position.z;
                this.storageInstance.params.rotation_x = BABYLON.Tools.ToDegrees(mesh.rotation.x);
                this.storageInstance.params.rotation_y = BABYLON.Tools.ToDegrees(mesh.rotation.y);
                this.storageInstance.params.rotation_z = BABYLON.Tools.ToDegrees(mesh.rotation.z);
                this.storageInstance.params.scale_x = mesh.scaling.x;
                this.storageInstance.params.scale_y = mesh.scaling.y;
                this.storageInstance.params.scale_z = mesh.scaling.z;
            }
        }
    }
}