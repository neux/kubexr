// XRImageHandler.ts
import * as BABYLON from '@babylonjs/core';
import { XRHandlerBase } from './XRHandlerBase';
import { XRStorage } from './XRStorage';
import { StorageInstance } from '../models/storage';

export class XRImageHandler extends XRHandlerBase {
    private guiVisible: boolean = false;

    constructor(scene: BABYLON.Scene, xrstorage: XRStorage) {
        super(scene, xrstorage);
    }

    getTypeId(): string {
        return "xrimage";
    }

    async createInstanceMesh(instance: StorageInstance): Promise<BABYLON.AbstractMesh> {
        const plane = BABYLON.MeshBuilder.CreatePlane(instance.id, { width: 1, height: 1 }, this.scene);
        const material = new BABYLON.StandardMaterial(instance.id + "_material", this.scene);
        material.diffuseTexture = new BABYLON.Texture(instance.params.image_url, this.scene);
        plane.material = material;
        this.updateInstanceMesh(plane, instance.params);
        return plane;
    }

    updateInstanceMesh(mesh: BABYLON.AbstractMesh, params: any): void {
        mesh.position = new BABYLON.Vector3(params.pos_x || 0, params.pos_y || 0, params.pos_z || 0);
        mesh.rotation = new BABYLON.Vector3(
            BABYLON.Tools.ToRadians(params.rotation_x || 0),
            BABYLON.Tools.ToRadians(params.rotation_y || 0),
            BABYLON.Tools.ToRadians(params.rotation_z || 0)
        );
        mesh.scaling = new BABYLON.Vector3(params.scale_x || 1, params.scale_y || 1, params.scale_z || 1);

        // Update texture if image_url has changed
        const material = mesh.material as BABYLON.StandardMaterial;
        if (material && material.diffuseTexture) {
            const texture = material.diffuseTexture as BABYLON.Texture;
            if (texture.url !== params.image_url) {
                material.diffuseTexture = new BABYLON.Texture(params.image_url, this.scene);
            }
        }
    }

    hideGui(): void {
        // Implement hide GUI logic
        this.guiVisible = false;
        console.log("XRImageHandler: GUI hidden");
    }

    showGui(): void {
        // Implement show GUI logic
        this.guiVisible = true;
        console.log("XRImageHandler: GUI shown");
    }

    toggleGui(): void {
        this.guiVisible = !this.guiVisible;
        console.log(`XRImageHandler: GUI toggled, now ${this.guiVisible ? 'visible' : 'hidden'}`);
    }

    isGuiVisible(): boolean {
        return this.guiVisible;
    }
}