import { Panel } from './models/panel';
import { Buttons } from './models/buttons';
import { KubeXrMap } from './elements/KubeXrMap';
import { KubeXrLevel } from './elements/KubeXrLevel';
import { KubeXrRow } from './elements/KubeXrRow';
import { Asset } from './elements/Asset';
import { Assets } from './elements/Assets';
import { CreativeMode } from './elements/CreativeMode';
import { GizmoManager } from './elements/GizmoManager';
import { GuiManager } from './elements/GuiManager';
import { SceneManager } from './elements/SceneManager';
import { XRHandlerBase } from './elements/XRHandlerBase';
import { XRCubeHandler } from './elements/XRCubeHandler';
import { XRImageHandler } from './elements/XRImageHandler';
import { XRStorage } from './elements/XRStorage';
import * as storage from './models/storage';
import * as storageXrCube from './models/storageXrCube';
import * as storageXrImage from './models/storageXrImage';

export const models = {
  Panel,
  Buttons
};

export const elements = {
  KubeXrMap,
  KubeXrLevel,
  KubeXrRow,
  Asset,
  Assets,
  CreativeMode,
  GizmoManager,
  GuiManager,
  SceneManager,
  XRHandlerBase,
  XRCubeHandler,
  XRImageHandler,
  XRStorage
};

export {
  Panel,
  Buttons,
  KubeXrMap,
  KubeXrLevel,
  KubeXrRow,
  Asset,
  Assets,
  CreativeMode,
  GizmoManager,
  GuiManager,
  SceneManager,
  XRHandlerBase,
  XRCubeHandler,
  XRImageHandler,
  XRStorage,
  storage,
  storageXrCube,
  storageXrImage
};

// Ensure these exports are not tree-shaken
const _unused = {
  Panel,
  Buttons,
  XRStorage,
  SceneManager,
  models,
  elements,
  storage,
  storageXrCube,
  storageXrImage
};
Object.keys(_unused).forEach(key => {
  if (typeof (_unused as any)[key] === 'undefined') {
    console.warn(`Export ${key} is undefined`);
  }
});
