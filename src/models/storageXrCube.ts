import { StorageType, guiFolderPositionRotationScale, guiFolderPersistenceConfig } from './storage';

export const storageXrCube: StorageType = {
    id: "cube",
    gui_name: "XR Cube Instances",
    gui_folders: [guiFolderPositionRotationScale, guiFolderPersistenceConfig]
};
