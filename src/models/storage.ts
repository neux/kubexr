export interface ParamConfig {
    param: string;
    min?: number;
    max?: number;
    step?: number;
    type?: string;
    readonly?: boolean;
    handler?: string;
    default?: any;
    output?: boolean;
}

export interface GUIFolder {
    id: string;
    name: string;
    params: ParamConfig[];
}

export interface StorageType {
    id: string;
    gui_name: string;
    gui_folders: GUIFolder[];
}

export interface StorageInstance {
    id: string;
    type: string;
    params: Record<string, any>;
}

export const guiFolderPositionRotationScale: GUIFolder = {
    id: 'pos_rot_sc',
    name: "Position, Rotation, Scale",
    params: [
        { param: "pos_x", min: -100, max: 100, step: 0.1, default: 0 },
        { param: "pos_y", min: -100, max: 100, step: 0.1, default: 0 },
        { param: "pos_z", min: -100, max: 100, step: 0.1, default: 0 },
        { param: "rotation_x", min: 0, max: 360, step: 1, default: 0 },
        { param: "rotation_y", min: 0, max: 360, step: 1, default: 0 },
        { param: "rotation_z", min: 0, max: 360, step: 1, default: 0 },
        { param: "scale_x", min: 0.1, max: 10, step: 0.1, default: 1 },
        { param: "scale_y", min: 0.1, max: 10, step: 0.1, default: 1 },
        { param: "scale_z", min: 0.1, max: 10, step: 0.1, default: 1 }
    ]
};

export const guiFolderPersistenceConfig: GUIFolder = {
    id: 'storage',
    name: "Storage",
    params: [
        { param: "id", type: "textbox", readonly: true, output: false },
        { param: "Save", type: "button", handler: "saveInstance", output: false },
        { param: "Delete", type: "button", handler: "deleteInstance", output: false }
    ]
};