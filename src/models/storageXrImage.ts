import { StorageType, guiFolderPositionRotationScale, guiFolderPersistenceConfig, GUIFolder } from './storage';

const guiFolderImageConfig: GUIFolder = {
    id: 'image_config',
    name: "Image Config",
    params: [
        { param: "image_url", type: "textbox", handler: "updateImage", output: true }
    ]
};

export const storageXrImage: StorageType = {
    id: "image",
    gui_name: "XR Image Instances",
    gui_folders: [
        guiFolderPositionRotationScale,
        guiFolderImageConfig,
        {
            ...guiFolderPersistenceConfig,
            params: [
                ...guiFolderPersistenceConfig.params,
                { param: "image_asset", type: "dropdown", handler: "selectImageAsset", output: true }
            ]
        }
    ]
};
