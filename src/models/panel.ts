export class Panel  {
    inputs: Record<string, (args: Record<string, any>) => void>;
    data: {
        object: {
            inputs: Record<string, any>;
        };
    };

    constructor() {
        this.inputs = {
            merge: this.onPanelInputsMerge,
            replace: this.onPanelInputsReplace
        };
        this.data = {
            object: {
                inputs: {}
            }
        };
    }

    private onPanelInputsMerge(args: Record<string, any>): void {
        console.log("onPanelInputsMerge: ", args);
    }

    private onPanelInputsReplace(args: Record<string, any>): void {
        console.log("onPanelInputsReplace: ", args);
    }
}
