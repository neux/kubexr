import * as BABYLON from '@babylonjs/core';
import * as BabylonjsGui from '@babylonjs/gui';

export class Buttons {
    private advancedTexture: BabylonjsGui.AdvancedDynamicTexture;
    private panel?: BabylonjsGui.StackPanel;
    private buttons: Map<string, BabylonjsGui.Button> = new Map();

    constructor(private scene: BABYLON.Scene) {
        this.advancedTexture = BabylonjsGui.AdvancedDynamicTexture.CreateFullscreenUI("UI", true, this.scene);
        this.createButtonPanel();
    }

    private createButtonPanel(): void {
        const container = new BabylonjsGui.Rectangle();
        container.thickness = 0;
        container.horizontalAlignment = BabylonjsGui.Control.HORIZONTAL_ALIGNMENT_CENTER;
        container.verticalAlignment = BabylonjsGui.Control.VERTICAL_ALIGNMENT_TOP;
        container.height = "40px";
        container.width = "100%";
        container.top = "20px";
        container.isPointerBlocker = true;
        this.advancedTexture.addControl(container);

        this.panel = new BabylonjsGui.StackPanel();
        this.panel.isVertical = false;
        container.addControl(this.panel);
    }

    public addButton(typeId: string, onClick: (type: string) => void): void {
        console.log(`Adding button for ${typeId}`);
        const buttonText = typeId.charAt(0).toUpperCase() + typeId.slice(1);
        const button = BabylonjsGui.Button.CreateSimpleButton(`button_${typeId}`, buttonText);
        button.width = "80px";
        button.height = "30px";
        button.color = "white";
        button.background = "blue";

        button.onPointerUpObservable.add(() => {
            onClick(typeId);
        });

        this.panel?.addControl(button);
        this.buttons.set(typeId, button);
        console.log(`Button for ${typeId} added successfully`);
    }

    public dispose(): void {
        if (this.advancedTexture) {
            this.advancedTexture.dispose();
        }

        this.buttons.forEach(button => {
            this.panel?.removeControl(button);
            button.dispose();
        });

        this.buttons.clear();

        if (this.panel?.parent) {
            this.panel.parent.removeControl(this.panel);
        }

        this.panel?.dispose();
    }
}
